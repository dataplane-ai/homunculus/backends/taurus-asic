# *************************************************************************
#
# Copyright 2023 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

from homunculus.utils import helpers

class Runner():

    registry_name = "dnn"
    extensible_path = None

    @classmethod
    def generate(cls, base_dir, model_data):
        helpers.makeLUTsFromModel(model_data["model"], base_dir + "/luts")
        cls.generateSpatial(model_data, base_dir)

    @classmethod
    def generateSpatial(cls, model_data, base_dir, num_pkts=1):

        sp = helpers.Spatial()
        lut_dir = base_dir + "/luts"
        num_fields = model_data['input_dim']
        arch = model_data['arch']
        pars = model_data["pars"]

        sp.setupImports("spatial.dsl._", "spatial.lib.ML._",
                        "utils.io.files._",
                        "spatial.lang.{FileBus,FileEOFBus}",
                        "spatial.metadata.bounds._")

        sp.setupClass(model_data["name"])

        sp.setupConstants({"N": num_pkts,
                           "field": num_fields,
                           "project_dir": "s\"" + lut_dir + "\""})
        sp.setupTypes({"T": "Float"})
        sp.setupMain()

        for i in range(len(arch)):
            w_path = "s\"" + lut_dir + "/L" + str(i) + "_NEURON_W_LUT.csv\""
            b_path = "s\"" + lut_dir + "/L" + str(i) + "_NEURON_B_LUT.csv\""
            dim2 = num_fields if i == 0 else arch[i - 1]

            mems = {}
            mems["L" + str(i) + "_res"] = {
                                    "mem_type": "SRAM",
                                    "data_type": "T",
                                    "dims": [arch[i]]}

            mems["L" + str(i) + "_W_LUT"] = {
                                    "mem_type": "FileLUT",
                                    "data_type": "T",
                                    "dims": [arch[i], dim2],
                                    "path": w_path}

            mems["L" + str(i) + "_B_LUT"] = {
                                    "mem_type": "FileLUT",
                                    "data_type": "T",
                                    "dims": [arch[i]],
                                    "path": b_path}

            sp.setupMemories(mems)

        sp.startStream("N", "p")
        sp.setupMemories({"packet": {
                                "mem_type": "SRAM",
                                "data_type": "T",
                                "dims": [num_fields]}})

        sp.foreach(num_fields, min(16, num_fields), "f",
                   ["packet(f) = stream_in.value"])

        for i in range(len(arch)):

            neuron_size = num_fields if i == 0 else arch[i - 1]
            arg_mem = "packet" if i == 0 else ("L" + str(i - 1) + "_res")

            
            sp.buildDNNLayer({"type": "T",
                              "num_neurons": arch[i],
                              "neuron_size": neuron_size,
                              "arg_mem": arg_mem,
                              "result_mem": "L" + str(i) + "_res",
                              "outer_par": min(pars[i], arch[i]),
                              "inner_par": min(16, neuron_size),
                              "weight_lut": "L" + str(i) + "_W_LUT",
                              "bias_lut": "L" + str(i) + "_B_LUT"})
            
        last_elem = model_data["output_dim"]
        last_idx = len(arch)-1

        sp.chooseMultiClassResult("T", last_elem, min(16, last_elem),
                                  "L" + str(last_idx) + "_res", "decision")

        sp.endStream("decision", "p")
        sp.closeBraces()

        sp.makeSpatialFile(base_dir, model_data["name"])
