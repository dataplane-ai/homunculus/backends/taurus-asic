# *************************************************************************
#
# Copyright 2023 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

all: spatial

SCALA_FILE ?= tests/FFMult.scala

deps:
	./deps.sh

spatial: 
	git clone https://gitlab.com/dataplane-ai/spatial.git
	cd spatial && git checkout pir --
	
	# Patch spatial
	patch -u spatial/Makefile -i patches/spatial/Makefile.patch
	patch -u spatial/src/spatial/PlasticineTest.scala -i patches/spatial/PlasticineTest.scala.patch
	# Build spatial
	cd spatial && make pir

	cd spatial/pir && git checkout homunculus
	# Patch pir
	patch -u spatial/pir/Makefile -i patches/spatial/pir/Makefile.patch
	patch -u spatial/pir/requirements.txt -i patches/spatial/pir/requirements.txt.patch
	rm -f spatial/pir/regression/SimpleSparse.scala
	# Build pir
	cd spatial/pir && make

test:
	cp $(SCALA_FILE) spatial/pir/regression
	cd spatial && bin/test -a ffmult -b Tst -p pirTest -T -s --stat
	
clean:
	find . | grep -E "(__pycache__|\.pyc)" | xargs rm -rf

.PHONY: spatial deps
