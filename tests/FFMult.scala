/* *************************************************************************
#
# Copyright 2023 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************/

import spatial.dsl._
import spatial.lib.ML._
import utils.io.files._
import spatial.lang.{FileBus,FileEOFBus}
import spatial.metadata.bounds._

@spatial class ffmult extends SpatialTest {

        val BITWIDTH = 8
        val MSB = 1 << (BITWIDTH - 1)
        val MAX_VAL = (1 << BITWIDTH) - 1
	
        val N = 1
	type T = I32

	def main(args: Array[String]): Unit = {

        val infile_a = buildPath(IR.config.genDir,"tungsten", "in_a.csv")
        val infile_b = buildPath(IR.config.genDir,"tungsten", "in_b.csv")
		
	val outfile = buildPath(IR.config.genDir,"tungsten", "out.csv")
	
        createDirectories(dirName(infile_a))
	createDirectories(dirName(infile_b))
	val inputs = List.tabulate(N) {i => i % (1 << BITWIDTH)}
	    
        writeCSVNow(inputs, infile_a)
        writeCSVNow(inputs, infile_b)

	val stream_a_in  = StreamIn[T](FileBus[T](infile_a))
	val stream_b_in  = StreamIn[T](FileBus[T](infile_b))
        stream_a_in.count = N
        stream_b_in.count = N

	val stream_out  = StreamOut[Tup2[I32,Bit]](FileEOFBus[Tup2[I32,Bit]](outfile))


        Accel {

            val irred = 0x11b // Irreducible polynomial of GF(2^8) 
            val mask_a = MSB
            val mask_b = 1
            val mask_bitwidth = MAX_VAL
           
            def multStage( fifo_in_tuple: (FIFO[I32],FIFO[I32],FIFO[I32]),
                           fifo_out_tuple: (FIFO[I32],FIFO[I32],FIFO[I32])
                         ): Unit = {
  
                val a = fifo_in_tuple._1.deq()
                val b = fifo_in_tuple._2.deq()
                val result_in = fifo_in_tuple._3.deq()

                val flag_b = ( b & mask_b ) == mask_b
                val result_out = mux(flag_b, result_in ^ a, result_in)
                fifo_out_tuple._3.enq(result_out & mask_bitwidth)

                val flag_a = ( a & mask_a ) == mask_a 
                val a_out = mux(flag_a, (a << 1) ^ irred, a << 1)
                fifo_out_tuple._1.enq(a_out & mask_bitwidth)
                
                val b_out = b >> 1
                fifo_out_tuple._2.enq(b_out & mask_bitwidth)
            }


            val NUM_STAGES = BITWIDTH
            val fifos = List.tabulate(NUM_STAGES + 1){ i =>
                ( FIFO[T](4), FIFO[T](4), FIFO[T](4) )
            }

            Stream.Foreach(N by 1) { p =>
            
                Pipe {
                    fifos(0)._1.enq(stream_a_in.value)
                    fifos(0)._2.enq(stream_b_in.value)
                    fifos(0)._3.enq(0)
                }
                
                List.tabulate(NUM_STAGES){ stage =>
                    multStage(fifos(stage), fifos(stage+1))
                } 

                Pipe {
                    // STREAM OUT
                    val stage = NUM_STAGES
                    stream_out := Tup2(fifos(stage)._3.deq(), p == (N-1))
                    fifos(stage)._1.deq()
                    fifos(stage)._2.deq()
                }
            }
	}
	assert(true)
    }
}
