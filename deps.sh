# *************************************************************************
#
# Copyright 2023 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

# NOTE: The given deps.sh has been tested on Ubuntu 20.04 LTS

#  Install jdk 8 and utils
sudo apt-get update
sudo apt-get -y install openjdk-8-jdk
sudo apt-get -y install jq curl nano wget git make python-is-python3 -y
sudo apt-get -y clean && sudo apt-get -y autoremove

# Install spatial dependencies
sudo apt-get install -y pkg-config libgmp3-dev libisl-dev

# Install g++-9 from testing repository
sudo apt-get install -y build-essential software-properties-common
sudo add-apt-repository ppa:ubuntu-toolchain-r/test -y
sudo apt-get update -y
sudo apt-get install gcc-9 g++-9 -y
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 60 --slave /usr/bin/g++ g++ /usr/bin/g++-9
sudo apt-get -y clean && sudo apt-get -y autoremove

# Install sbt
sudo echo "deb https://repo.scala-sbt.org/scalasbt/debian all main" | sudo tee /etc/apt/sources.list.d/sbt.list
sudo echo "deb https://repo.scala-sbt.org/scalasbt/debian /" | sudo tee /etc/apt/sources.list.d/sbt_old.list
sudo curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | sudo apt-key add
sudo apt-get update -y
sudo apt-get install -y sbt

# Python dependencies
sudo apt-get install -y python3.8-venv python3-dev

# Other stuff
sudo apt-get install -y libglib2.0-dev graphviz
