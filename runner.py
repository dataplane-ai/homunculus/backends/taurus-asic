# *************************************************************************
#
# Copyright 2023 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import os
import importlib
import shutil
import subprocess as sp
from homunculus.utils import helpers
from homunculus.backends.registry import BackendRegistry
from homunculus.backends.backend import Backend


class Runner(BackendRegistry, Backend):

    registry_name = "taurus-asic"
    extensible_path = "algorithms"

    def __init__(self, algorithm, constraints, resources):
        
        self.constraints = constraints
        self.resources = resources
        self.lines = None

        path = os.path.dirname(os.path.abspath(__file__)) + "/"
        print(path)
        runner_spec=importlib.util.spec_from_file_location("runner", path + self.extensible_path + "/" + algorithm + "/runner.py")
        runner_module = importlib.util.module_from_spec(runner_spec)

        runner_spec.loader.exec_module(runner_module)
        self.generator = getattr(runner_module, "Runner")

        self.spatial_path = os.path.dirname(os.path.abspath(__file__)) + "/spatial/"
        self.homunculus_dir = self.spatial_path + "pir/regression/homunculus"
        os.makedirs(self.homunculus_dir, exist_ok=True)

        return

    @classmethod
    def checkResources(cls, resources):
        return

    @classmethod
    def checkConstraints(cls, constraints):
        return

    def generate(self, model_data):
        app_dir = helpers.setupGenDir(self.homunculus_dir, model_data["name"])
        self.generator.generate(app_dir, model_data)

    def run(self, model_names):

        num_threads = str(len(model_names) * 4)
        apps = " ".join("-a " + mn for mn in model_names)
        cmd = ["bin/test",
               "-t", num_threads, apps,
               "-b", "Tst", "-p", "pirTest", "-T",
               "-s", "--stat", "--row",
               str(self.resources["rows"]), "--col",
               str(self.resources["cols"])]

        p = sp.Popen(cmd, cwd=self.spatial_path, stdin=sp.PIPE, stdout=sp.PIPE, encoding="utf-8")
        while True:
            output = p.stdout.readline()
            if output == '' and p.poll() is not None:
                break
            if output:
                print(output.strip())

        return

    def checkFeasibility(self, model_data, print_summary=False):
        print("Generating Taurus Code for model \"" +
              model_data["name"] + "\": ")

        self.generate(model_data)
        lines = self.run([model_data["name"]])
        success = False
        latency = 0
        throughput = 0

        for line in lines:
            if ("Not enough resource of type") in line:
                success = False
                break

            if ("All tests passed.") in line:
                print("\033[1A" + Fore.GREEN +
                      "Generating Taurus Code for model \"" +
                      model_data["name"] +
                      "\": PIR compilation/simulation successful" +
                      Fore.CYAN)
                success = True

        if (success is False):
            print("\033[1A" + Fore.RED +
                  "Generating Taurus Code for model \"" +
                  model_data["name"] +
                  "\": PIR compilation/simulation failed" +
                  Fore.CYAN)
            return success

        if (print_summary):
            out = sp.check_output(["bin/log", "gen/Tst_pirTest",
                                  model_data["name"], "-m", "PCU,PMU"],
                                  cwd=self.spatial_path,
                                  stderr=sp.STDOUT).decode()
            pcus = ""
            pmus = ""
            latency = ""
            app = ""
            for line in out.splitlines():
                report = line.split(" ")
                if (len(report) > 10) and (report[1] == model_data["name"]):
                    app = report[1]
                    pcus = report[2].split(":")[1]
                    pmus = report[3].split(":")[1]
                    latency = report[10].split(":")[1]

            print("========================")
            print("Output: ")
            print("App: " + app)
            print("PCUs: " + pcus)
            print("PMUs: " + pmus)
            print("Latency: " + latency)
            print("Metric Value: " + str(model_data["metric_value"]))
            print("========================")

        shutil.rmtree(self.spatial_path + "/gen/Tst_pirTest/")
        
        return success

    def checkFeasibilityPar(self, models, print_summary=False):
        print("Generating Taurus Code for model set")

        names = []
        for model_data in models:
            self.generate(model_data)
            names.append(model_data["name"])

        self.run(names)
        parse_fields = "succeeded,runp2p_cycle,PCU,PMU"
        out = sp.check_output(["bin/log", "gen/Tst_pirTest",
                              model_data["name"], "-m", parse_fields],
                              cwd=self.spatial_path,
                              stderr=sp.STDOUT).decode()

        latency = ""
        app = ""

        valids = [False] * len(models)
        res_use = [(0, 0)] * len(models)
        for idx, model in enumerate(models):
            for line in out.splitlines():
                report = line.split(" ")
                found = ((len(report) > 2) and
                         (report[0] == "Tst") and
                         (report[1] == model["name"]))
                if found:
                    print(line)
                    if ("error" in line) or ("bug" in line):
                        continue
                    else:

                        try: 
                            valids[idx] = True
                            app = report[1]
                            success = report[2].split(":")[1]
                            pcus = int(report[3].split(":")[1].split("\x1b")[0])
                            pmus = int(report[4].split(":")[1].split("\x1b")[0])
                            res_use[idx] = (pcus, pmus)
                            print("PCUs: " + str(res_use[idx][0]))
                            print("PMUs: " + str(res_use[idx][1]))
                        except:
                            valids[idx] = False
                            res_use[idx] = (0, 0)
                        
        '''
        if (print_summary):
            print("========================")
            print("Output: ")
            print("App: " + app)
            print("PCUs: " + pcus)
            print("PMUs: " + pmus)
            print("Latency: " + latency)
            print("Metric Value: " + str(model_data["metric_value"]))
            print("========================")
        '''

        shutil.rmtree(self.spatial_path + "/gen/Tst_pirTest")

        return valids, res_use
